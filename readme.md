Installation:

    git clone git@bitbucket.org-BrendonReed:BrendonReed/dotfiles.git ~/dotfiles
    on windows
    git clone git@bitbucket.org-BrendonReed:BrendonReed/dotfiles.git ~/dotfiles

Create symlinks or set pointer for vim:

    ln -s ~/dotfiles/.vim ~/.vim
    ln -s ~/.vim/vimrc ~/.vimrc
    ln -s ~/.vim/gvimrc ~/.gvimrc
    on windows
    mklink vimfiles dotfiles/vimfiles
    mklink _vimrc vimfiles\vimrc
    mklink _gvimrc vimfiles\gvimrc
    mklink _vsvimrc vimfiles\vsvimrc
    mklink /D .vim dotfiles/vimfiles

Install ag (the silver surfer) in PATH

Install vim plugins by running :PlugInstall in vim.

for other symlinks:

```
stow bash
stow zsh
stow git
```
